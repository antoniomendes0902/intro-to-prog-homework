#1st CLASS

print('Olá Mundo')


#2nd CLASS


stock_name="AAPL"
stock_value=0.5
shares=10
action="sell"
print("I would like to",action,shares,"shares in",stock_name)

person1="António"
person2="Laura"
loan=10
stocks=2
company_name="AAPL"
print(person1,"needs to borrow",loan,"euros from",person2,"in order to trade",stocks,"stocks in",company_name)

number_1=10
number_2=15
total=number_1*number_2
print("multiplicaton =",total)

celsius=30
fahrenheit=(celsius*9/5)+32
kelvin=celsius+273.15
print(celsius,"C degrees is equal to",fahrenheit,"f and",kelvin,"k")

#3rd CLASS - Functions

def power(a,b) :
    return (a**b)
a=2
b=3
print(a,"at the power of",b,"is equal to",power(a,b))

def equalizer(number_1, number_2):
    return(number_1==number_2)
number_1=7
number_2=power(a,b)
print(number_1,"and",number_2, "are equal -",equalizer(number_1, number_2))

def hypotenuse(side_1, side_2):
    return(((side_1**2)+(side_2**2))**0.5)
side_1=8
side_2=6
print("using the the pythagoras theorem we get that the hypotenuse is equal to", hypotenuse(side_1, side_2))

bitcoin=65

def litecoin(bitcoin):
    blite_rate=114.77
    return bitcoin*blite_rate
print(bitcoin, "bitcoins is equal to",litecoin(bitcoin),"litecoins")

def ethereum(bitcoin):
    beth_rate=29.4
    return bitcoin*beth_rate
print(bitcoin, "bitcoins is equal to",ethereum(bitcoin),"ethers") 

def euros(bitcoin):
    beuros_rate=5654.96
    return bitcoin*beuros_rate
print(bitcoin, "bitcoins is equal to",euros(bitcoin),"euros")

def no_return():
    return 
print(no_return())
type(no_return())
####Returns none -- none type. Same happens when you don explicitly put the return statement####


#4th CLASS - Keyword arguments + Scope


global_scope=10
def function_with_no_args():
    return global_scope*2
a=function_with_no_args()
print(global_scope)
print(a)

def name(first_name="António", second_name="Mendes"):
    print("My name is", first_name, second_name)
name()
name("António", )  
first_name="ana"
second_name="trincao"
name(first_name=first_name, second_name=second_name)
name(first_name, second_name)

def greetings(message="Good morning", nome=""):
    print(message + nome)
greetings(message="You know nothing ", nome="John Snow")

#5th CLASS - DATA STRUCTURES


AAPL_tuple=(65.55,109.65,92.65,118.07,165.53)
print("AAPL stock price in the last 5y is:", AAPL_tuple)

AAPL_list=[5.91,8.96,16.24,17.97,18.59,32.90]
print("AAPL stock price in 2005-2010 is:", AAPL_list)
AAPL_list.append(42.08)
print("AAPL stock price in 2005-2011 is:",AAPL_list)

def lister(a_tuple):
    return [a_tuple]

my_list=["a","e","o","b","c"]

my_dict={
        "hello":"world",
        "whats":"up",
        }

def appender(a_list):
    (a_list.append("homie"))
    return a_list

GOOG=110
AAPL=100
KPMG=90
BCG=30
MSFT=23
OSYS=37
PHZR=86
def average(a,b,c,d,e,f,g):
    return (a+b+c+d+e+f+g)/7
print(average(GOOG,AAPL,KPMG,BCG,MSFT,OSYS,PHZR))
stocks=[GOOG, AAPL, KPMG, BCG, MSFT, OSYS, PHZR]   
def average_one(a):
    return sum(a)/7
print(average_one(stocks))

#6th class - Flow control

def truth(ticker_symbol):
    if ticker_symbol=="AAPL":
        return True
    else:
        return False
    
def pink(gender):
    if gender=="male":
        return "you like pink"
    if gender=="female":
        return "you hate pink"
  
def pink(gender):
    if gender=="male":
        print("you like pink")
    if gender=="female":
        print("you hate pink")
    print("this should not be printed")
#it always prints the last statement because there is no if next to it. if we put else:print.. it won't print when using male or female

def age(father_age, mother_age):
    if father_age==mother_age:
       return father_age + mother_age
    else:
        return max(father_age, mother_age)-min(father_age, mother_age)
  
def size(a_dict, a_list):
    if len(a_list)==len(a_dict):
        return("awesome")
    else:
        return "that is sad"
  
a_dict={
        "apple":100,
        "google":59,
        "facebook":77
        }
for key in a_dict:
    print(key)
for key in a_dict:
    print(a_dict[key])
for key in a_dict:
    print("The stock price of company", key, "is", a_dict[key])
    
a_tuple=(10,20,30)
a_list=[40,50,60]
other_dict={
        "AAPL":100,
        "GOOG":90,
        "FB":80}
for num in a_tuple:
    print (num)
for num in a_list:
    print(num)
for key in other_dict:
    print(other_dict[key])
    
def lister (a_list):
    new_list=[]
    for num in a_list:
        new_list.append(num*2)
    return new_list

def diction (a_dict):
    new_dict={}
    for key in a_dict:
        new_dict[key]=2*a_dict[key]
    return new_dict

def length (number, a_list):
    if len(a_list)==number:
        return True
        
            