#1
class Robot:
    def __init__(self):
        self.recordings=[]
    def listen(self, string):
        self.recordings.append(string)
    def play_recordings(self):
        print(self.recordings)
    def delete_recordings(self):
        self.recordings.clear()
        
robot_1=Robot()
robot_2=Robot()
robot_1.listen("hello")
robot_1.listen("world")
robot_2.listen("goodbye")
robot_1.play_recordings()

#2
robot_2.play_recordings()
robot_1.delete_recordings()
robot_1.play_recordings()
robot_1.listen("hi")
robot_1.play_recordings()

#4
class LectureRoom:
    def __init__(self):
        self.capacity=40
    def increase_capacity(self, amount):
        if self.capacity+amount<100 and self.capacity+amount>10:
            self.capacity=self.capacity+amount
        else:
            print("error - you cannot increase to that capacity")
    def decrease_capacity(self, amount):
        if self.capacity-amount>10 and self.capacity-amount>100:
            self.capacity=self.capacity-amount
        else:
            print("you cannot decrease to that capacity")
class_1=LectureRoom()
class_1.increase_capacity(80)
print(class_1.capacity)
class_1.decrease_capacity(10)
print(class_1.capacity)
        