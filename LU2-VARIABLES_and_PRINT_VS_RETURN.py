#ex1

stock_name="AAPL"
stock_value=0.5
shares=10
action="sell"
print("I would like to",action,shares,"shares in",stock_name)

#ex2

person1="António"
person2="Laura"
loan=10
stocks=2
company_name="AAPL"
print(person1,"needs to borrow",loan,"euros from",person2,"in order to trade",stocks,"stocks in",company_name)

#ex 3

number_1=10
number_2=15
total=number_1*number_2
print("multiplicaton =",total)

#ex 4

celsius=30
fahrenheit=(celsius*9/5)+32
kelvin=celsius+273.15
print(celsius,"C degrees is equal to",fahrenheit,"f and",kelvin,"k")
