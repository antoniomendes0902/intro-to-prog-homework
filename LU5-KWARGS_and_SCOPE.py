#4

global_scope=10
def function_with_no_args():
    return global_scope*2
a=function_with_no_args()
print(global_scope)
print(a)

#9

def name(first_name="António", second_name="Mendes"):
    print("My name is", first_name, second_name)
name()
name("António", )  
first_name="ana"
second_name="trincao"
name(first_name=first_name, second_name=second_name)
name(first_name, second_name)

#11

def greetings(message="Good morning", nome=""):
    print(message + nome)
greetings(message="You know nothing ", nome="John Snow")

