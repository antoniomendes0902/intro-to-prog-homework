from novamud import Dungeon, Room, Thing

class Ticket(Thing):
    name="ticket"
    description="you need to buy tickets to get around"

class Alameda(Room):
    name="Alameda"
    description="you are at Alameda and need to get to Cais Metro Station"
    
    def init_room(self):
        self.add_thing(Ticket())
    
    def go_to(self, player, other_room_name):
        if not player.carrying or player.carrying.name !="ticket":
            player.tell("you can't travel for free! You need to get a ticket")
        else:
            super().go_to(player, other_room_name)
        
class cais_metro(Room):
    name="CaisMetro"
    description="you are at Cais Metro and need to get to Cais train Station"

class cais_train_station(Room):
    name="CaisTrainStation"
    description="you need to get to Oeiras"
    
    def init_room(self):
        self.add_thing(Ticket())
    
    def go_to(self, player, other_room_name):
        if not player.carrying or player.carrying.name !="ticket":
            player.tell("you can't travel for free! You need to get a ticket")
        else:
            super().go_to(player, other_room_name)

class Oeiras(Room):
    name='OeirasTrainStation'
    description='Welcome to Oeiras, you are getting closer to campus! ps: do not forget to swipe your ticket'
    
    def register_commands(self):
        return ['swipe_the_ticket']
    
    def add_player(self, player):
        player.ticket_swipe = 0
        super().add_player(player)
    
    def swipe_the_ticket(self, player):
        player.ticket_swipe = 1
        player.tell("Ticket swiped")
    
    def swipe_the_ticket_describe(self):
        return ("Swipe the ticket here to open the gate")
        
    def go_to(self, player, other_room_name):
        
        if player.ticket_swipe==0:
            player.tell("Ups someone forgot to swipe the ticket")
        
        else:
            super().go_to(player,other_room_name)

class outside(Room):
    name="outside"
    description ="its nice and warm, you can walk to the university, 15 more steps and you will be there"
    
    def register_commands(self):
        return ("walk"),("walk_back")
    def add_player(self, player):
        player.steps=0
        super().add_player(player)
    def walk(self, player):
        player.steps +=1
        if 15-player.steps>0:
            player.tell("Come on you are {} steps away".format(15-player.steps))
        elif 15-player.steps<0:
            player.tell("You walked too much. You have to go {} steps back".format(player.steps-15))
        else:
            player.tell("You arrived! just enter Nova")
    def walk_back(self, player):
        player.steps -=1
        if 15-player.steps>0:
            player.tell("Come on you are {} steps away".format(15-player.steps))
        elif 15-player.steps<0:
            player.tell("You walked too much. You have to go {} steps back".format(player.steps-15))
        else:
            player.tell("You arrived! just enter Nova")
    def walk_back_describe(self):
        return "if you went too far you have to go back"
    def walk_describe(self):
        return "you need to walk to reach NOVA"
    def go_to(self, player, other_room_name):
        if player.steps==15:
            super().go_to(player,other_room_name)
        else:
            player.tell("you still have some walking to do")
class Nova(Room):
    name="Nova"
    description="what a beautiful university. You now need to enter the classroom"

    def register_commands(self):
         return ['call_sam']
    
    def add_player(self, player):
        player.door='Closed'
        super().add_player(player)
    
    def call_sam(self,player):
        player.door='Open'
        player.tell("You should go in now or you'll miss the JOD")
    
    def call_sam_describe(self):
        return ("Only Sam can open the door, call him!")
    
    
    def go_to(self, player, other_room_name):
        
        if player.door=='Open':
            super().go_to(player,other_room_name)
    
class ClassRoom(Room):
    name="ClassRoom"
    description="you have finally made it!! time to learn new stuff"
    
    def register_commands(self):
        return ["learn"]
    def learn(self, player):
        player.tell("congratulations you won the game!!!!!")
    def learn_describe(self):
        return ("time to learn!!")
    

        
        
class Lisbon(Dungeon):

    name = 'Lisbon'
    description = ("you are in Lisbon at Alameda and you need to get to nova for the intro to prog class")

    def init_dungeon(self):
        al = Alameda(self)
        cm = cais_metro(self)
        cts = cais_train_station(self)
        oe = Oeiras(self)
        ou = outside(self)
        no = Nova(self)
        cl= ClassRoom(self)
        al.connect_room(cm, two_way=True)
        cm.connect_room(cts, two_way=True)
        cts.connect_room(oe, two_way=True)
        oe.connect_room(ou, two_way=True)
        ou.connect_room(no, two_way=True)
        no.connect_room(cl, two_way=True)
        self.start_room = al


if __name__ == '__main__':
   Lisbon().start_dungeon()

