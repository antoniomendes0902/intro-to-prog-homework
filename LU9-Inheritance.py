# 1

class LectureRoom:

    def __init__(self):
        self.capacity = 40
        self.min_capacity = 40
        self.max_capacity = 100
        
    def valid_capacity(self, new_capacity):
        if new_capacity < self.min_capacity or new_capacity > self.max_capacity:
            print('invalid capacity, must be between',
                  self.min_capacity, 'and', self.max_capacity)
            return False
        else:
            return True

    def increase_capacity(self, amount):
        if self.valid_capacity(self.capacity + amount):
            self.capacity += amount

    def decrease_capacity(self, amount):
        if self.valid_capacity(self.capacity - amount):
            self.capacity -= amount




class BigRoom(LectureRoom):
    
    def __init__(self):
        self.capacity = 40
        self.min_capacity = 50
        self.max_capacity = 200


class SmallRoom(LectureRoom):
    
    def __init__(self):
        self.capacity = 10
        self.min_capacity = 5
        self.max_capacity = 20


###################################################
# 2

class Robot:
    
    def __init__(self):
        self.recordings = []
    
    def listen(self, string):
        self.recordings.append(string)
    
    def play_recordings(self):
        print(self.recordings)
        
    def delete_recordings(self):
        self.recordings = []
        

class FlameThrower(Robot):
    
    def throw_flames(self):
        print('Burn, baby! Burn!!!')


class Tank(FlameThrower):
    
    def throw_flames(self):
        print('Throws VERY LARGE flame')
        
    def hulk_stomp(self):
        print('Hulk stomping everywhere!')

###################################################
# 3

class Robot:
    
    def __init__(self):
        self.recordings = []
    
    def listen(self, string):
        self.recordings.append(string)
    
    def play_recordings(self):
        print(self.recordings)
        
    def delete_recordings(self):
        self.recordings = []
        

class LightRobot(Robot):
    
    def listen(self, string):
        super().listen(string)
        if len(self.recordings) > 2:
            self.recordings.remove(self.recordings[0])

class FlameThrower(LightRobot):
    
    def throw_flames(self):
        print('Burn, baby! Burn!!!')
        self.delete_recordings()


class CryingRobot(LightRobot):
    
    def listen(self, string):
        super().listen(string)
        if len(self.recordings) >= 3:
            print('Disk is sooooo fullll. Call the wambulance!')


class FlyingRobot(LightRobot):
    
    def fly(self):
        print('I am flying gracefully in the clouds!')
        del self.recordings[-1]
