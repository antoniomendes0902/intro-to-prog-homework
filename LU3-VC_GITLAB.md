Hello world!
# Links

[gitlab](https://gitlab.com)

# Code Formatting
```py
print("hello"+" World")
```

# Lists example
## ordered
1. review previous class
1. do the homework
1. save on gitlab

## unordered
* save on gitlab
+ review previous class
- do the homework

## mixed
1. review previous class
2. do the execises
    * LU1
    * LU2
* study for the exam
    1. review all slides
    2. do all exercises